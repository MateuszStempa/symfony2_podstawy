<?php

namespace Eduweb\TrainingBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;

class NotificationService
{
    private $session;

    function __construct(Session $session)
    {
        $this->session = $session;
    }

    protected function assMessage($type, $message)
    {
        $this->session->getFlashBag()->add($type, $message);
    }

        public function addSuccess($message)
    {
        $this->assMessage('success', $message);
    }

    public function addError($message)
    {
        $this->assMessage('danger', $message);
    }
}