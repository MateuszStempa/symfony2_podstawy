<?php

namespace Eduweb\TrainingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/eduweb")
 */

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EduwebTrainingBundle:Default:index.html.twig', array('name' => $name));
    }

    public function registerUserAction($name, $age, $role)
    {
        $responseMsg = sprintf('Rejestracja użytkownika o imieniu %s (wiek: %d), rola w systemie: %s',$name, $age, $role);
        return new \Symfony\Component\HttpFoundation\Response($responseMsg);
    }

    public function simple1Action()
    {
        return new \Symfony\Component\HttpFoundation\Response('Simple 1');
    }

    public function simple2Action()
    {
        return new \Symfony\Component\HttpFoundation\Response('Simple 2');
    }

    /**
     * @Route(
     *      "/register-tester/{name}-{age}-{role}",
     *      name="eduweb_training_registerTester",
     *      defaults={"role"="units"},
     *      requirements={"age"="\d+", "role"="units|functional"}
     * )
     *
     * @Method({"GET"})
     */
    public function registerTesterAction($name, $age, $role)
    {
        $responseMsg = sprintf('Rejestracja testera o imieniu %s (wiek: %d), rola w systemie: %s',$name, $age, $role);
        return new \Symfony\Component\HttpFoundation\Response($responseMsg);
    }

    /**
     * @Route("/anno")
     */
    public function anno1Action()
    {
        return new \Symfony\Component\HttpFoundation\Response('Adnotacja 1');
    }
    /**
     * @Route("/anno")
     */
    public function anno2Action()
    {
        return new \Symfony\Component\HttpFoundation\Response('Adnotacja 2');
    }
    /**
     * @Route("/generate-url")
     */
    public function generateUrlAction()
    {
        $response = $this->generateUrl('eduweb_training_registerUser', array(
            'name' => 'Marcin',
            'age' => 21,
            'country' => 'Poland'
        ), TRUE);
        return new \Symfony\Component\HttpFoundation\Response($response);
    }
    /**
     * @Route("/debugging", name="eduweb_training_debugging")
     */
    public function debuggingAction()
    {
        return new \Symfony\Component\HttpFoundation\Response('<html><head><title>Debugging</title></head><body>Debugging</body></html>');
    }
}
