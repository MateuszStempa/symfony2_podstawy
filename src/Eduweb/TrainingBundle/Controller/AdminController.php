<?php

namespace Eduweb\TrainingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Eduweb\TrainingBundle\Form\Type\RegisterType;


/**
 * @Route("/blog/admin")
 *
 * @author Mateusz Stempa <stempamateusz@gmail.com>
 */
class AdminController extends Controller
{

    /**
     * @Route(
     *      "/",
     *      name="edu_blog_admin_listing"
     * )
     * 
     * @Template
     */
    public function listingAction()
    {

        $repo = $this->getDoctrine()->getRepository('EduwebTrainingBundle:Register');
        $rows = $repo->findAll();
//        $rows = $repo->findBy(['country' => 'AD']);

        if($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $btns = TRUE;
        } else {
            $btns = FALSE;
        }

//        $user = $this->getUser();

        return ['rows' => $rows, 'btns' => $btns];
    }

    /**
     * @Route(
     *      "/details/{id}",
     *      name="edu_blog_admin_details"
     * )
     *
     * @Template
     */
    public function detailsAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('EduwebTrainingBundle:Register');
        $register = $repo->find($id);

        if(null == $register){
            throw $this->createNotFoundException('Nie znaleziono taiej rejestracji na szkolenie!');
        }
        return [
            'register' => $register
        ];
    }

    /**
     * @Route(
     *      "/update/{id}",
     *      name="edu_blog_admin_update"
     * )
     *
     * @Template
     */
    public function updateAction(Request $Request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('EduwebTrainingBundle:Register');
        $register = $repo->find($id);

        if(null == $register){
            throw $this->createNotFoundException('Nie znaleziono taiej rejestracji na szkolenie!');
        }

        $form = $this->createForm(new RegisterType(), $register);

        if($Request->isMethod('POST')){

            $session = $this->get('session');
            $form->handleRequest($Request);

            if($form->isValid()){

                $em = $this->getDoctrine()->getManager();
                $em->persist($register);
                $em->flush();

                $session->getFlashBag()->add('success', 'Zaktualizowano rekord.');

                return $this->redirect($this->generateUrl('edu_blog_admin_details', [
                    'id' => $register->getId()
                ]));

            } else {

                $session->getFlashBag()->add('danger', 'Popraw błędy formularza!');

            }

        }

        return [
            'register' => $register,
            'form' => $form->createView()
        ];
    }

    /**
     * @Route(
     *      "/delete/{id}",
     *      name="edu_blog_admin_delete"
     * )
     */
    public function deleteAction($id)
    {
        $repo = $this->getDoctrine()->getRepository('EduwebTrainingBundle:Register');
        $register = $repo->find($id);

        if(null == $register){
            throw $this->createNotFoundException('Nie znaleziono taiej rejestracji na szkolenie!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($register);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Poprawnie usunięto rekord z bazy danych!');

        return $this->redirect($this->generateUrl('edu_blog_admin_listing'));

    }

}