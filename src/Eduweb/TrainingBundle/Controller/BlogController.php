<?php

namespace Eduweb\TrainingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;


use Eduweb\TrainingBundle\Helper\Journal\Journal;
use Eduweb\TrainingBundle\Helper\DataProvider;
use Eduweb\TrainingBundle\Form\Type\RegisterType;
use Eduweb\TrainingBundle\Entity\Register;

/**
 * @Route("/blog")
 *
 * @author Mateusz Stempa <stempamateusz@gmail.com>
 */
class BlogController extends Controller
{

    /**
     * @Route(
     *      "/",
     *      name="edu_blog_glowna"
     * )
     * 
     * @Template
     */
    public function indexAction()
    {
//        throw new Exception('nowy błąd');
        return [];
    }

    /**
     * @Route(
     *      "/dziennik",
     *      name="edu_blog_dziennik"
     * )
     *
     * @Template
     */
    public function journalAction()
    {
        return [
//            'history' => Journal::getHistoryAsArray()
            'history' => Journal::getHistoryAsObjects()
//            'history' => []
        ];
    }

    /**
     * @Route(
     *      "/kontakt",
     *      name="edu_blog_contact"
     * )
     *
     * @Template
     */
    public function contactAction()
    {
        return [];
    }

     /**
     * @Route(
     *      "/ksiega-gosci",
     *      name="edu_blog_guestBook"
     * )
     *
     * @Template
     */
    public function guestBookAction()
    {
        return [
            'comments' => DataProvider::getGuestBook()
        ];
    }

    /**
     * @Route(
     *      "/rejestracja",
     *      name="edu_blog_register"
     * )
     *
     * @Template
     */
    public function registerAction(Request $Request)
    {
        $Register = new Register();
        $Register->setName('Mateusz PHPDEV')
            ->setEmail('matii.php.dev@gmail.com')
            ->setCountry('PL')
            ->setCourse('basic')
            ->setInvest(['a','o']);

        $Session = $this->get('session');

        if(!$Session->has('registered'))
        {
        
        $form = $this->createForm(new RegisterType(), $Register);

        $form->handleRequest($Request);

        

        if($Request->isMethod('POST')){

            if($form->isValid()){

                $savePath = $this->get('kernel')->getRootDir().'/../web/uploads/';
                $Register->save($savePath);

                $em = $this->getDoctrine()->getManager();
                $em->persist($Register);
                $em->flush();

                $msgBody = $this->renderView('EduwebTrainingBundle:Email:base.html.twig', ['name' => $Register->getName()]);

                $message = \Swift_Message::newInstance()
                    ->setSubject('Potwierdzenie rejestracji')
                    ->setFrom(['matii.php.dev@gmail.com' => 'Mateusz PHP.DEV'])
                    ->setTo([$Register->getEmail() => $Register->getName()])
                    ->setBody($msgBody, 'text/html');

                $this->get('mailer')->send($message);

//                $Session->getFlashBag()->add('success', 'Twoje zgloszenie zostało zapisane!');

                    $this->get('edu_notification')->addSuccess('Twoje zgłoszenie zostało zapisane!');

                $Session->set('registered', true);

                return $this->redirect($this->generateUrl('edu_blog_register'));

            }  else {

//                $Session->getFlashBag()->add('danger', 'Popraw błędy formularza.');

                $this->get('edu_notification')->addError('Popraw błędy formularza!');

            }
        }
    }

    return [
            'form' => isset($form) ? $form->createView() : NULL
        ];
    }

    /**
     * @Template("EduwebTrainingBundle:Blog/Widgets:followingWidget.html.twig")
     */
    public function followingWidgetAction()
    {
        return [
            'list' => DataProvider::getFollowings()
        ];
    }

    /**
     * @Template("EduwebTrainingBundle:Blog/Widgets:walletWidget.html.twig")
     */
    public function walletWidgetAction()
    {
        return [
            'list' => DataProvider::getWallet()
        ];
    }
}