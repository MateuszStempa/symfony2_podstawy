<?php

namespace Eduweb\TrainingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class PagesController extends Controller
{

    /**
     * @Route(
     *      "/about",
     *      name="eduweb_training_aboutPage"
     * )
     *
     * @Template
     */
    public function aboutAction()
    {
//        return new Response('about');
//        $json = [
//            'name' => 'Buty',
//            'size' => '42',
//            'price' => '249,99'
//        ];
//        return new Response(json_encode($json), Response::HTTP_OK,['Content-type' => 'application/json']);
//        $content = $this->renderView('EduwebTrainingBundle:Pages:about.html.twig');
//        return new Response($content);
//        return $this->render('EduwebTrainingBundle:Pages:about.html.twig', ['name' => 'Mateusz']);
//        return [];

        return ['name' => 'Piotrek'];
    }

    /**
     * @Route("/go-to-page")
     */
    public function goToPageAction()
    {
//        return $this->redirect('http://www.google.pl');

        $redirectUrl = $this->generateUrl('eduweb_training_registerTester',
            ['name' => 'Mateusz', 'age' => 28]);
        return $this->redirect($redirectUrl);
    }

    /**
     * @Route("/print-header/{title}/{color}")
     *
     * @Template
     */
    public function printHeaderAction($title, $color)
    {
        return ['title' => $title, 'color' => $color];
    }

    /**
     * @Route("/contact")
     */
    public function contactPageAction()
    {
        return $this->forward('EduwebTrainingBundle:pages:printHeader', ['title'=>'Kontakt','color'=>'blue']);
    }

    /**
     * @Route("/generate-error")
     */
    public function generateErrorAction()
    {
//        throw $this->createNotFoundException('Ta strona nie została znaleziona!');

        throw new \Exception('Wystąpił błąd aplikacji.');
    }

    /**
     * @Route(
     *      "/mastering-request/{name}",
     *      name="eduweb_training_masteringRequest"
     * )
     */
    public function masteringRequestAction(Request $Request, $name)
    {
//        $request = $this->getRequest();  //nie będzie działać w wersji 3.0
//        $request = $this->get('request');

//        return new Response($Request->query->get('kolor', 'red'));
        return new Response($Request->request->get('size', '123'));
    }

    /**
     * @Route("/read-params")
     */
    public function readParamsAction()
    {
        $param = $this->container->getParameter('appApiKey');
        return new Response($param);
    }
}