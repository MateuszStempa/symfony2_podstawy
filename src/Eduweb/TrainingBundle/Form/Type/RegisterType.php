<?php
namespace Eduweb\TrainingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterType extends AbstractType
{
    public function getName()
    {
        return 'register_form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text', [
                'label' => 'Imię i nazwisko'
            ])
            ->add('email', 'email', [
                'label' => 'Email'
            ])
            ->add('sex', 'choice', [
                'choices' => [
                    'm' => 'Mężczyzna',
                    'k' => 'Kobieta'
                ],
                'label' => 'Płeć',
                'expanded' => true
            ])
            ->add('birthdate', 'birthday', [
                'label' => 'Data urodzenia',
                'empty_value' => '--',
                'empty_data' => NULL
            ])
            ->add('country', 'country', [
                'label' => 'Kraj',
                'empty_value' => '--',
                'empty_data' => NULL
            ])
            ->add('course', 'choice', [
                'choices' => [
                    'basic' => 'Kurs podstawowy',
                    'at' => 'Analiza techniczna',
                    'af' => 'Analiza fundamentalna',
                    'master' => 'Kurs zaawansowany'
                ],
                'label' => 'Kurs',
                'empty_value' => '--',
                'empty_data' => NULL
            ])
            ->add('invest', 'choice', [
                'choices' => [
                    'a' => 'Akcje',
                    'o' => 'Obligacje',
                    'f' => 'Forex',
                    'etf' => 'ETF'
                ],
                'label' => 'Inwestycje',
                'expanded' => true,
                'multiple' => true
            ])
            ->add('comments', 'textarea', [
                'label' => 'Uwagi'
            ])
            ->add('paymentFile', 'file', [
                'label' => 'Potwierdzenie przelewu'
            ])
            ->add('rules', 'checkbox', [
                'label' => 'Akceptuję regulamin',
                'constraints' => [
                    new Assert\NotBlank()
                ],
                'mapped' => false
            ])
            ->add('save', 'submit', [
                'label' => 'Zapisz'
            ]);
    }

    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Eduweb\TrainingBundle\Entity\Register'
        ]);
    }

}